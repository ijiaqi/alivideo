package com.aliyun.svideo.editor.effects.caption.listener;

import android.graphics.PointF;

import com.aliyun.svideosdk.common.AliyunColor;
import com.aliyun.svideosdk.common.AliyunFontStyle;
import com.aliyun.svideosdk.common.AliyunTypeface;
import com.aliyun.svideosdk.editor.impl.AliyunPasterControllerCompoundCaption;

public interface OnCaptionChooserStateChangeListener {
    void onCaptionTextChanged(String text);

    void onCaptionTextColorChanged(AliyunColor aliyunColor);

    void onCaptionTextFontStyleChanged(AliyunFontStyle fontStyle);

    void onCaptionTextFontTypeFaceChanged(AliyunTypeface aliyunTypeface);

    void onCaptionTextFontTtfChanged(String fontPath);

    void onCaptionTextFontSizeChanged(float fontSize);

    void onCaptionTextStrokeColorChanged(AliyunColor aliyunColor);

    void onCaptionTextStrokeWidthChanged(int width);

    void onCaptionTextShandowColorChanged(AliyunColor aliyunColor);

    void onCaptionTextShandowOffsetChanged(PointF shadowOffset);

    void onBubbleEffectTemplateChanged(String bubbleDir,String fontDir);

    void onFontEffectTemplateChanged(String template);

    void onCaptionFrameAnimation(int animationIndex);

    void onCaptionCancel();

    void onCaptionConfirm();

    AliyunPasterControllerCompoundCaption getAliyunPasterController();

    boolean isInvert();


}
