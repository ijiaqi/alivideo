package com.aliyun.svideo.editor.util;

import android.util.Log;
import android.view.View;
import android.view.ViewParent;

import com.aliyun.svideo.editor.contant.CaptionConfig;
import com.aliyun.svideosdk.common.struct.effect.ActionBase;
import com.aliyun.svideosdk.common.struct.effect.ActionFade;
import com.aliyun.svideosdk.common.struct.effect.ActionScale;
import com.aliyun.svideosdk.common.struct.effect.ActionTranslate;
import com.aliyun.svideosdk.common.struct.effect.ActionWipe;

public class CaptionFrameAnimationUtil {
    private static final String TAG = "CaptionFrameAnimationUt";

    public static ActionBase createAction(int animIndex, long duration, long startTimeUs, int displayWidth, int displayHeight,
                                          int pasterPostionX, int pasterPostionY) {
        Log.d(TAG, "createAction: pasterWidth:" + pasterPostionX + "  pasterHeight:" + pasterPostionY);
        ActionBase actionBase = null;
        switch (animIndex) {
            case CaptionConfig.EFFECT_NONE:
                actionBase = null;
                break;
            case CaptionConfig.EFFECT_UP:
                ActionTranslate upActionTranslate = new ActionTranslate();
                setActionFromPoint(displayWidth, displayHeight, pasterPostionX, pasterPostionY, upActionTranslate);
                upActionTranslate.setToPointY(1f);
                upActionTranslate.setToPointX(upActionTranslate.getFromPointX());
                actionBase = upActionTranslate;
                break;
            case CaptionConfig.EFFECT_RIGHT:
                ActionTranslate rightActionBase = new ActionTranslate();
                setActionFromPoint(displayWidth, displayHeight, pasterPostionX, pasterPostionY, rightActionBase);
                rightActionBase.setToPointX(1f);
                rightActionBase.setToPointY(rightActionBase.getFromPointY());
                actionBase = rightActionBase;
                break;
            case CaptionConfig.EFFECT_LEFT:
                ActionTranslate leftActionTranslate = new ActionTranslate();
                setActionFromPoint(displayWidth, displayHeight, pasterPostionX, pasterPostionY, leftActionTranslate);
                leftActionTranslate.setToPointX(-1f);
                leftActionTranslate.setToPointY(leftActionTranslate.getFromPointY());
                actionBase = leftActionTranslate;
                break;
            case CaptionConfig.EFFECT_DOWN:
                ActionTranslate downActionTranslate = new ActionTranslate();
                setActionFromPoint(displayWidth, displayHeight, pasterPostionX, pasterPostionY, downActionTranslate);
                downActionTranslate.setToPointY(-1f);
                downActionTranslate.setToPointX(downActionTranslate.getFromPointX());
                actionBase = downActionTranslate;
                break;
            case CaptionConfig.EFFECT_SCALE:
                actionBase = new ActionScale();
                ((ActionScale) actionBase).setFromScale(1f);
                ((ActionScale) actionBase).setToScale(0.25f);
                break;
            case CaptionConfig.EFFECT_LINEARWIPE:
                actionBase = new ActionWipe();
                ((ActionWipe) actionBase).setWipeMode(ActionWipe.WIPE_MODE_DISAPPEAR);
                ((ActionWipe) actionBase).setDirection(ActionWipe.DIRECTION_RIGHT);
                break;
            case CaptionConfig.EFFECT_FADE:
                actionBase = new ActionFade();
                ((ActionFade) actionBase).setFromAlpha(1.0f);
                ((ActionFade) actionBase).setToAlpha(0.2f);
                break;
            default:
                break;
        }


        if (actionBase != null) {
            actionBase.setDuration(duration);
            actionBase.setStartTime(startTimeUs);
        }

        return actionBase;
    }

    private static void setActionFromPoint(int displayWidth, int displayHeight, int pasterPostionX, int pasterPostionY, ActionTranslate actionTranslate) {
        float xRadio = pasterPostionX * 1.0f / displayWidth;
        float yRadio = pasterPostionY * 1.0f / displayHeight;
        float fromPointX = xRadio * 2 - 1;
        float fromPotinY = 1 - yRadio * 2;
        actionTranslate.setFromPointX(fromPointX);
        actionTranslate.setFromPointY(fromPotinY);
    }

}
