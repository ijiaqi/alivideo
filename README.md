[![](https://jitpack.io/v/com.gitlab.ijiaqi/alivideo.svg)](https://jitpack.io/#com.gitlab.ijiaqi/alivideo)
# ALiVideo
**阿里趣视频方案<br/>**
**使用：**
    <br/>implementation 'cn.dayulong.alivideo:alivideo:1.1.5'
**版本更新说明：**
<br/>1.1.7：修复作品播放列表取消关注时状态不更新的问题
<br/>1.1.6：作品播放完成、结束、暂停时返回作品Id及当前播放进度
<br/>1.1.5：作品播放列表关注状态维护添加用户分类
<br/>1.1.4：添加没有视图的上传（VodFilePushManager）