package cn.dayulong.alivideo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import com.aliyun.apsara.alivclittlevideo.net.data.LittleMineVideoInfo
import com.aliyun.apsara.alivclittlevideo.utils.HttpClientUtil
import com.aliyun.apsara.alivclittlevideo.view.FollowAnimationButton
import com.aliyun.apsara.alivclittlevideo.view.video.AlivcVideoPlayView
import com.aliyun.apsara.alivclittlevideo.view.video.BaseVideoSourceModel
import com.aliyun.apsara.alivclittlevideo.view.video.VideoViewClickCallBack
import com.aliyun.apsara.alivclittlevideo.view.video.weight.MenuClick
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.ToastUtils
import com.google.gson.reflect.TypeToken
import com.sackcentury.shinebuttonlib.ShineButton
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class PlayOpusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_play_opus)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        HttpClientUtil.HOST_API = "http://test.dayulong.cn/"
        findViewById<AlivcVideoPlayView>(R.id.video_play).apply {
            setBottomCommentVisible(View.GONE)
            val videoPlay = findViewById<AlivcVideoPlayView>(R.id.video_play)
            videoPlay.apply {
                onResume()
                val screenWidth =
                    com.aliyun.svideo.common.utils.ScreenUtils.getWidth(this@PlayOpusActivity)
                val screenHeight =
                    com.aliyun.svideo.common.utils.ScreenUtils.getRealHeight(this@PlayOpusActivity)
                if (screenHeight / screenWidth > 2) {
                    val viewHeight =
                        (screenHeight - com.aliyun.svideo.common.utils.ScreenUtils.getNavigationHeight(
                            this@PlayOpusActivity
                        )).toInt()
                    val layoutParams: ViewGroup.LayoutParams = layoutParams
                    layoutParams.height = viewHeight
                    setLayoutParams(layoutParams)
                }

                val json = getJsonByAssets("opus_list.json")
                val type = GsonUtils.getListType(object :
                    TypeToken<LittleMineVideoInfo.VideoListBean>() {}.type)
                val list =
                    GsonUtils.fromJson<MutableList<LittleMineVideoInfo.VideoListBean>>(json, type)
                refreshVideoList(list)
                setOnVideoViewClickListener(object : VideoViewClickCallBack() {
                    override fun playEnd(opusId: String?, progress: Int) {
                        super.playEnd(opusId, progress)
                        Log.e("playEnd", "opusId:$opusId progress:$progress")
                    }

                    override fun onClickView(view: View?, model: BaseVideoSourceModel?) {
                        super.onClickView(view, model)
                        when (view?.id) {
                            R.id.iv_avatar -> {
                                videoDownLoad(model)
                            }
                        }
                    }

                    override fun onClickView(
                        view: View,
                        menuClick: MenuClick?,
                        model: BaseVideoSourceModel?
                    ) {
                        when (menuClick) {
                            MenuClick.ATTENTION -> {
                                if (view is FollowAnimationButton) {
                                    view.startFollowAnimation()
                                }
                            }
                        }
                    }

                    override fun onHeartClick(
                        heartButton: ShineButton?,
                        isChecked: Boolean,
                        model: BaseVideoSourceModel?
                    ) {
                        super.onHeartClick(heartButton, isChecked, model)
                        model?.apply {
                            support = 99
                            grade = 5f
                            userCommentTotal = 33
                            forwardingNumber = 66
                            title =
                                "用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用不剧透哦放心食用"
                        }
                        ToastUtils.showShort(currentModel.teacherName)
                        refreshData(model)
                    }
                })
            }
        }
    }

    /**
     * 读取本地Json
     */
    fun getJsonByAssets(fileName: String): String {
        val context = ActivityUtils.getTopActivity()
        val sb = StringBuilder()
        try {
            val manage = context.assets
            val br = BufferedReader(InputStreamReader(manage.open(fileName)))
            var line: String? = ""
            while ((br.readLine().also { line = it }) != null) {
                sb.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return sb.toString()
    }
}