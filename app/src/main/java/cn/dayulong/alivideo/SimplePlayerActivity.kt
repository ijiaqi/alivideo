package cn.dayulong.alivideo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.aliyun.apsara.alivclittlevideo.sts.StsInfoManager
import com.aliyun.player.IPlayer
import com.aliyun.player.alivcplayerexpand.constants.GlobalPlayerConfig
import com.aliyun.player.alivcplayerexpand.widget.AliyunVodPlayerView
import com.aliyun.player.source.VidSts
import com.aliyun.player.VidPlayerConfigGen
import com.aliyun.player.alivcplayerexpand.view.trailers.TrailersView
import com.aliyun.svideo.common.utils.ToastUtils


class SimplePlayerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_player)
        findViewById<AliyunVodPlayerView>(R.id.video_course).apply {
            setOnCompletionListener {
                ToastUtils.show(context, "播放完成")
                showTrailersViewEnd()
            }
            setOnTrailerViewClickListener(object : TrailersView.OnTrailerViewClickListener {
                override fun onTrailerPlayAgainClick() {
                }

                override fun onOpenVipClick() {
                    setControlBarCanShow(true)
                    playAfterPurchase()
                }
            })
            //试看
//            setTrailerTime(6)
//            setPlayDomain("vod.dayulong.cn")
//            GlobalPlayerConfig.IS_TRAILER = true
//            GlobalPlayerConfig.IS_VIDEO = true
            setTitleBarCanShow(false)
            setAutoPlay(false)
            //创建VidSts
            val aliyunVidSts = VidSts().apply {
                val tokenInfo = StsInfoManager.getInstance().stsToken
                vid = "ded82a6ffde54cb1b3130a85724a727e"
                accessKeyId = tokenInfo.accessKeyId
                accessKeySecret = tokenInfo.accessKeySecret
                securityToken = tokenInfo.securityToken
                region = tokenInfo.region
                val configGen = VidPlayerConfigGen()
                configGen.setPreviewTime(6) //20秒试看
                setPlayConfig(configGen) //设置给播放源
            }
            //设置播放源
            setVidSts(aliyunVidSts)
        }
    }
}