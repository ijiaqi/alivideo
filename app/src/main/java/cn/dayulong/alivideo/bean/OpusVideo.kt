package cn.dayulong.alivideo.bean

import com.aliyun.apsara.alivclittlevideo.constants.LittleVideoParamConfig
import com.aliyun.apsara.alivclittlevideo.sts.StsInfoManager
import com.aliyun.apsara.alivclittlevideo.view.video.BaseVideoSourceModel
import com.aliyun.apsara.alivclittlevideo.view.video.videolist.VideoSourceType
import com.aliyun.player.source.VidSts

/**
 *  @ProjectName :ALiVideo
 *  @Author : QI.JIA
 *  @Time : 2021/11/2 13:52
 *  @Description :
 */
class OpusVideo : BaseVideoSourceModel() {
    var videoId: String? = null
    override fun getSourceType(): VideoSourceType {
        return VideoSourceType.TYPE_STS
    }

    override fun getUUID(): String {
        return id
    }

    override fun getVidStsSource(): VidSts? {
        val tokenInfo = StsInfoManager.getInstance().stsToken
        //生成播放需要信息
        val vidSource = VidSts()
        if (tokenInfo != null) {
            vidSource.accessKeyId = tokenInfo.accessKeyId
            vidSource.accessKeySecret = tokenInfo.accessKeySecret
            vidSource.securityToken = tokenInfo.securityToken
            vidSource.region = LittleVideoParamConfig.Player.REGION
        }
        vidSource.vid = videoId
        vidSource.title = getTitle()
        return vidSource
    }
}