package cn.dayulong.alivideo

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.aliyun.apsara.alivclittlevideo.constants.LittleVideoParamConfig
import com.aliyun.apsara.alivclittlevideo.net.LittleHttpManager
import com.aliyun.apsara.alivclittlevideo.view.publish.AlivcVideoPublishView
import com.aliyun.apsara.alivclittlevideo.view.publish.OnAuthInfoExpiredListener
import com.aliyun.apsara.alivclittlevideo.view.video.OnUploadCompleteListener
import com.aliyun.svideo.editor.EditorMediaActivity
import com.aliyun.svideo.editor.bean.AlivcEditInputParam
import com.aliyun.svideo.recorder.activity.AlivcSvideoRecordActivity
import com.aliyun.svideo.recorder.bean.AlivcRecordInputParam
import com.aliyun.svideo.recorder.bean.RenderingMode
import com.blankj.utilcode.util.LogUtils
import java.lang.ref.WeakReference

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<TextView>(R.id.tv_player).apply {
            setOnClickListener {
                startActivity(Intent("cn.dayulong.alivideo.SIMPLE_PLAYER"))
            }
        }
        findViewById<TextView>(R.id.tv_player_opus).apply {
            setOnClickListener {
                startActivity(Intent("cn.dayulong.alivideo.PLAY_OPUS"))
            }
        }
        findViewById<TextView>(R.id.tv_record_video).apply {
            setOnClickListener {
                jumpToRecorder()
            }
        }
        findViewById<TextView>(R.id.tv_choose_video).apply {
            setOnClickListener {
                jumpToEditor()
            }
        }
    }

    /**
     * 上传结果回调
     */
    private class MyUploadCompleteListener internal constructor(activity: MainActivity) :
        OnUploadCompleteListener {
        var weakReference: WeakReference<MainActivity>? = WeakReference(activity)
        override fun onSuccess(videoId: String, imageUrl: String, describe: String) {
            if (weakReference == null) {
                return
            }
            val activity = weakReference?.get()
            LogUtils.e("onSuccess")
        }

        override fun onFailure(code: String, msg: String) {
            if (weakReference == null) {
                return
            }
            val activity = weakReference!!.get()
            //标识位，恢复初始状态

            LogUtils.e("onFailure")
        }

    }

    /**
     * 权限列表
     */
    var permission = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val PERMISSION_REQUEST_CODE = 1001

    /**
     * 跳转到视频编辑 在[LittleVideoParamConfig.Editor] 中进行配置
     */
    private fun jumpToEditor() {
        val checkResult =
            com.aliyun.svideo.common.utils.PermissionUtils.checkPermissionsGroup(this, permission)
        if (!checkResult) {
            com.aliyun.svideo.common.utils.PermissionUtils.requestPermissions(
                this,
                permission,
                PERMISSION_REQUEST_CODE
            )
            return
        }
        val param = AlivcEditInputParam.Builder()
            .setRatio(LittleVideoParamConfig.Editor.VIDEO_RATIO)
            .setScaleMode(LittleVideoParamConfig.Editor.VIDEO_SCALE)
            .setVideoQuality(LittleVideoParamConfig.Editor.VIDEO_QUALITY)
            .setFrameRate(LittleVideoParamConfig.Editor.VIDEO_FRAMERATE)
            .setGop(LittleVideoParamConfig.Editor.VIDEO_GOP)
            .build()
        EditorMediaActivity.startImport(this, param)
    }

    /**
     * 跳转到视频录制界面, 不经过视频参数设置, 参数取默认值
     * 在[LittleVideoParamConfig.Recorder]
     * 中进行配置
     */
    private fun jumpToRecorder() {
        val recordInputParam = AlivcRecordInputParam.Builder()
            .setResolutionMode(LittleVideoParamConfig.Recorder.RESOLUTION_MODE)
            .setRatioMode(LittleVideoParamConfig.Recorder.RATIO_MODE)
            .setMaxDuration(LittleVideoParamConfig.Recorder.MAX_DURATION)
            .setMinDuration(LittleVideoParamConfig.Recorder.MIN_DURATION)
            .setVideoQuality(LittleVideoParamConfig.Recorder.VIDEO_QUALITY)
            .setGop(LittleVideoParamConfig.Recorder.GOP)
            .setVideoCodec(LittleVideoParamConfig.Recorder.VIDEO_CODEC)
            .setVideoRenderingMode(RenderingMode.Race)
            .build()
        AlivcSvideoRecordActivity.startRecord(this, recordInputParam)
    }
}