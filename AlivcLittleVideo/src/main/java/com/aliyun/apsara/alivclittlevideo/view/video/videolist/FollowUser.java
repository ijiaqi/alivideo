package com.aliyun.apsara.alivclittlevideo.view.video.videolist;

import android.text.TextUtils;

import androidx.annotation.Nullable;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2022/4/26 11:14
 * @Description :关注用户模型
 */
public class FollowUser {
    String userId;
    int userType;

    public FollowUser(String userId, int userType) {
        this.userId = userId;
        this.userType = userType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        FollowUser other = (FollowUser) obj;
        assert other != null;
        return TextUtils.equals(this.userId, other.userId) && this.userType == other.userType;
    }
}
