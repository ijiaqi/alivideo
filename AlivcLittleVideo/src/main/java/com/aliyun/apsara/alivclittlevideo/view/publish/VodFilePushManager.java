package com.aliyun.apsara.alivclittlevideo.view.publish;

import android.content.Context;

import com.alibaba.sdk.android.vod.upload.VODUploadCallback;
import com.alibaba.sdk.android.vod.upload.VODUploadClientImpl;
import com.alibaba.sdk.android.vod.upload.model.UploadFileInfo;
import com.alibaba.sdk.android.vod.upload.model.VodInfo;
import com.aliyun.apsara.alivclittlevideo.view.video.OnVodFileUploadListener;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2022/4/16 16:48
 * @Description :封装点播文件上传，仅支持单任务
 */
public class VodFilePushManager {
    private Context mContext;
    private OnVodFileUploadListener mOnVodFileUploadListener;
    private VODUploadClientImpl vodUploadClient;
    /**
     * 时间戳错误
     */
    private static final String TEIM_STAMP_ERROR = "InvalidTimeStamp.Expired";

    public VodFilePushManager(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnVodFileUploadListener(OnVodFileUploadListener onVodFileUploadListener) {
        this.mOnVodFileUploadListener = onVodFileUploadListener;
    }

    /**
     * 开始上传视频
     */
    public void startUploadVideo(
            String outputFilePath,
            String videoId,
            String videoAddress,
            String videoAuth,
            String videoDescribe) {
        vodUploadClient = new VODUploadClientImpl(mContext);
        VODUploadCallback callback = new VODUploadCallback() {
            @Override
            public void onUploadSucceed(UploadFileInfo info) {
                super.onUploadSucceed(info);
                if (mOnVodFileUploadListener != null) {
                    mOnVodFileUploadListener.onSuccess(videoId, "imageUrl", videoDescribe);
                }
            }

            @Override
            public void onUploadFailed(UploadFileInfo info, String code, String message) {
                super.onUploadFailed(info, code, message);
                if (TEIM_STAMP_ERROR.equals(code)) {
                    if (mOnVodFileUploadListener != null) {
                        mOnVodFileUploadListener.onFailure(code, message);
                    }
                }
            }

            @Override
            public void onUploadProgress(UploadFileInfo info, long uploadedSize, long totalSize) {
                super.onUploadProgress(info, uploadedSize, totalSize);
                if (mOnVodFileUploadListener != null) {
                    mOnVodFileUploadListener.onProgress(uploadedSize, totalSize);
                }
            }

            @Override
            public void onUploadStarted(UploadFileInfo uploadFileInfo) {
                super.onUploadStarted(uploadFileInfo);
                vodUploadClient.setUploadAuthAndAddress(uploadFileInfo, videoAuth, videoAddress);
                if (mOnVodFileUploadListener != null) {
                    mOnVodFileUploadListener.onStart();
                }
            }

            @Override
            public void onUploadRetry(String code, String message) {
                super.onUploadRetry(code, message);
            }
        };
        vodUploadClient.init(callback);
        VodInfo vodInfo = new VodInfo();
        vodInfo.setTitle("标题" + videoDescribe);
        vodInfo.setDesc("描述." + videoDescribe);
        vodUploadClient.addFile(outputFilePath, vodInfo);
        vodUploadClient.start();
    }

    public void cancel() {
        if (vodUploadClient != null) {
            vodUploadClient.cancelFile(0);
        }
    }
}
