package com.aliyun.apsara.alivclittlevideo.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;

import androidx.annotation.Nullable;

import com.aliyun.apsara.alivclittlevideo.R;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2021/12/10 18:11
 * @Description :
 */
public class FollowAnimationButton extends View {
    private float rectRadius;
    private RectF rect = new RectF();
    private Paint rectPaint = new Paint();
    private Paint plusPaint = new Paint();
    private float plusLineWidth;
    private Path plusPath = new Path();
    private Paint okPaint = new Paint();
    private float okLineWidth;
    private Path okPath = new Path();
    private PathMeasure pathMeasure;
    private boolean startDrawOk;
    private ValueAnimator animatorDrawOk;
    private PathEffect effect;
    private boolean isAnimating;
    private boolean isHide;

    public FollowAnimationButton(Context context) {
        super(context);
        init(context);
    }

    public FollowAnimationButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FollowAnimationButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setStartDrawOk(boolean drawOk) {
        isHide = drawOk;
    }

    private void init(Context context) {
        rectRadius = context.getResources().getDimension(R.dimen.dp_15);
        plusLineWidth = context.getResources().getDimension(R.dimen.dp_2);
        rectPaint.setAntiAlias(true);
        rectPaint.setColor(Color.parseColor("#00CBCC"));
        rectPaint.setStyle(Paint.Style.FILL);
        plusPaint.setAntiAlias(true);
        plusPaint.setColor(-1);
        plusPaint.setStyle(Paint.Style.STROKE);
        plusPaint.setStrokeWidth(this.plusLineWidth);
        okLineWidth = context.getResources().getDimension(R.dimen.dp_2);
        okPaint.setAntiAlias(true);
        okPaint.setColor(Color.parseColor("#00CBCC"));
        okPaint.setStyle(Paint.Style.STROKE);
        okPaint.setStrokeWidth(this.okLineWidth);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        float plusSize = getMeasuredHeight() * 0.6f;
        plusPath.moveTo(getMeasuredWidth() / 2f, (getMeasuredHeight() - plusSize) / 2f);
        plusPath.lineTo(getMeasuredWidth() / 2f, (getMeasuredHeight() + plusSize) / 2f);
        plusPath.moveTo((getMeasuredWidth() - plusSize) / 2f, getMeasuredHeight() / 2f);
        plusPath.lineTo((getMeasuredWidth() + plusSize) / 2f, getMeasuredHeight() / 2f);
        okPath.moveTo(getMeasuredWidth() * 0.27f, getMeasuredHeight() * 0.42f);
        okPath.lineTo(getMeasuredWidth() * 0.45f, getMeasuredHeight() * 0.68f);
        okPath.lineTo(getMeasuredWidth() * 0.69f, getMeasuredHeight() * 0.32f);
        pathMeasure = new PathMeasure(okPath, true);

//        62/17 62/26
//        62/28 62/42
//        62/43 62/20
    }

    protected void onDraw(Canvas canvas) {
        rect.left = 0;
        rect.top = 0;
        rect.right = getMeasuredWidth();
        rect.bottom = getMeasuredHeight();
        if (startDrawOk) {
            rectPaint.setColor(Color.WHITE);
            canvas.drawRoundRect(rect, rectRadius, rectRadius, rectPaint);
            canvas.drawPath(okPath, okPaint);
        } else {
            rectPaint.setColor(Color.parseColor("#00CBCC"));
//            canvas.drawRoundRect(rect, rectRadius, rectRadius, rectPaint);
            canvas.drawCircle(rect.right / 2, rect.right / 2, rect.right / 2, rectPaint);
            canvas.drawPath(plusPath, plusPaint);
        }
        if (isHide) setVisibility(GONE);

    }

    public final void startFollowAnimation() {
        this.isAnimating = true;
//        ScaleAnimation startScaleAnimation = new ScaleAnimation(1.0F, 1.2F, 1.0F, 1.2F, 1, 0.5F, 1, 0.5F);
//        startScaleAnimation.setDuration(300L);
//        startScaleAnimation.setAnimationListener((Animation.AnimationListener) (new Animation.AnimationListener() {
//            public void onAnimationStart(@Nullable Animation animation) {
//            }

//            public void onAnimationEnd(@Nullable Animation animation) {
        startDrawOk = true;
        animatorDrawOk = ValueAnimator.ofFloat(1, 0);
        animatorDrawOk.setDuration(1000);
        animatorDrawOk.addUpdateListener(animation1 -> {
            float value = (float) animation1.getAnimatedValue();
            float[] floatEffect = new float[]{pathMeasure.getLength(), pathMeasure.getLength()};
            effect = new DashPathEffect(floatEffect, value * pathMeasure.getLength());
            okPaint.setPathEffect(effect);
            invalidate();
        });
        animatorDrawOk.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                AnimationSet animationSet = new AnimationSet(true);
                AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0.2f);
                ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0.2f, 1f, 0.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animationSet.addAnimation(alphaAnimation);
                animationSet.addAnimation(scaleAnimation);
                animationSet.setDuration(300);
                animationSet.setInterpolator(new AccelerateInterpolator());
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        startDrawOk = false;
                        isAnimating = false;
                        setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                startAnimation(animationSet);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorDrawOk.start();
//            }

//            public void onAnimationRepeat(@Nullable Animation animation) {
//            }
//        }));
//        this.startAnimation((Animation) startScaleAnimation);
    }

    public final boolean isAnimating() {
        return this.isAnimating;
    }

}
