package com.aliyun.apsara.alivclittlevideo.view.video;

import android.view.View;
import android.widget.CompoundButton;

import com.aliyun.apsara.alivclittlevideo.net.data.LittleMineVideoInfo;
import com.aliyun.apsara.alivclittlevideo.view.video.weight.MenuClick;
import com.sackcentury.shinebuttonlib.ShineButton;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2021/11/3 17:00
 * @Description :
 */
public abstract class VideoViewClickCallBack implements AlivcVideoPlayView.OnVideoViewClickListener {
    @Override
    public void loadEnd(String opusId) {

    }

    @Override
    public void playEnd(String opusId, int progress) {
    }

    @Override
    public void onDeleteClick(LittleMineVideoInfo.VideoListBean videoId) {

    }

    @Override
    public void onClickView(View view, BaseVideoSourceModel model) {

    }

    @Override
    public void onClickView(View view, MenuClick menuClick, BaseVideoSourceModel model) {

    }

    @Override
    public void onChecked(CompoundButton buttonView, boolean isChecked, BaseVideoSourceModel model) {

    }

    @Override
    public void onHeartClick(ShineButton heartButton, boolean isChecked, BaseVideoSourceModel model) {

    }

}
