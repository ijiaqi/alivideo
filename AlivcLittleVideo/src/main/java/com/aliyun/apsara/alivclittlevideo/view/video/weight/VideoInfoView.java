package com.aliyun.apsara.alivclittlevideo.view.video.weight;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aliyun.apsara.alivclittlevideo.R;
import com.aliyun.apsara.alivclittlevideo.view.video.BaseVideoSourceModel;
import com.aliyun.svideo.common.utils.DensityUtils;
import com.aliyun.svideo.common.utils.ScreenUtils;
import com.sackcentury.shinebuttonlib.ShineButton;

/**
 * @Author : QI.JIA
 * @Time : 2022/1/8 16:30
 * @Description :视频（作品）信息, 教师信息, 用户名, 内容，进度条，评论框
 */
public class VideoInfoView extends LinearLayout implements View.OnClickListener, ShineButton.OnCheckedChangeListener {
    private LinearLayout teacherInfo;
    private ImageView teacherIcon;
    private TextView teacherCategory;
    private TextView teacherName;
    private TextView userName;
    private ExpandableTextView4List opusContent;
    private TextView opusTime;
    private OnInfoViewListener mListener;
    private final int CLOSE = 0;
    private final int OPEN = 1;
    private SparseIntArray maps = new SparseIntArray();
    private int mOpusTimeVisibility = View.GONE;

    public VideoInfoView(Context context) {
        this(context, null);
    }

    public VideoInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setOpusTimeVisible(int visible) {
        mOpusTimeVisibility = visible;
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_video_info_view, this, true);
        teacherInfo = view.findViewById(R.id.ll_teacher_info);
        teacherIcon = view.findViewById(R.id.iv_teacher_icon);
        teacherCategory = view.findViewById(R.id.tv_category);
        teacherName = view.findViewById(R.id.tv_teacher_name);
        userName = view.findViewById(R.id.tv_name);
        opusContent = view.findViewById(R.id.etv_content);
        opusTime = view.findViewById(R.id.tv_time);
        teacherInfo.setOnClickListener(this);
        teacherIcon.setOnClickListener(this);
        teacherCategory.setOnClickListener(this);
        teacherName.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    public void setVideoInfo(BaseVideoSourceModel info, int position) {
        teacherInfo.setVisibility(info.getType() == 1 ? View.VISIBLE : View.GONE);
        teacherCategory.setText(info.getCategoryName());
        teacherName.setText(info.getTeacherName());
        userName.setText("@ " + info.getNickName());
        final int status = maps.get(position, CLOSE);
        switch (status) {
            case CLOSE:
                opusContent.setText(info.getTitle(), false);
                break;
            case OPEN:
                opusContent.setText(info.getTitle(), true);
                break;
        }
        opusContent.setToggleListener(expanded -> {
            maps.put(position, expanded ? OPEN : CLOSE);
        });
        opusTime.setText(info.getCreationTime());
        opusTime.setVisibility(mOpusTimeVisibility);
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) return;
        int id = v.getId();
        if (id == R.id.ll_teacher_info ||
                id == R.id.iv_teacher_icon ||
                id == R.id.tv_category ||
                id == R.id.tv_teacher_name) {
            mListener.onClick(v, MenuClick.TEACHER);
        } else if (id == R.id.acb_silence) {
            mListener.onClick(v, MenuClick.SILENCE);
        }
    }

    @Override
    public void onCheckedChanged(View view, boolean checked) {
        if (mListener == null) return;
        mListener.onClick(view, checked ? MenuClick.LIKE : MenuClick.LIKE_UN);
    }

    public void setInfoListener(OnInfoViewListener listener) {
        mListener = listener;
    }

    public interface OnInfoViewListener {
        void onClick(View view, MenuClick menuClick);
    }
}
