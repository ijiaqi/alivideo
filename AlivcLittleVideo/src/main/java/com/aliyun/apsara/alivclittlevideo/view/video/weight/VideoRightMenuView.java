package com.aliyun.apsara.alivclittlevideo.view.video.weight;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aliyun.apsara.alivclittlevideo.R;
import com.aliyun.apsara.alivclittlevideo.utils.UrlUtils;
import com.aliyun.apsara.alivclittlevideo.view.FollowAnimationButton;
import com.aliyun.apsara.alivclittlevideo.view.video.BaseVideoSourceModel;
import com.aliyun.svideo.common.utils.image.ImageLoaderImpl;
import com.aliyun.svideo.common.utils.image.ImageLoaderOptions;
import com.sackcentury.shinebuttonlib.ShineButton;

/**
 * @Author : QI.JIA
 * @Time : 2022/1/8 16:30
 * @Description :菜单信息, 包含头像, 关注, 点赞，评论，分享
 */
public class VideoRightMenuView extends LinearLayout implements View.OnClickListener, ShineButton.OnCheckedChangeListener {
    private ImageView userIcon;
    private FollowAnimationButton userAttention;
    private LinearLayout userComment;
    private LinearLayout userShare;
    private ShineButton likeBtn;
    private TextView likeNumber;
    private ImageView commentIcon;
    private TextView commentNumber;
    private ImageView shareIcon;
    private TextView shareNumber;
    private OnMenuViewClick mListener;

    public VideoRightMenuView(Context context) {
        this(context, null);
    }

    public VideoRightMenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_video_right_menu_view, this, true);
        userIcon = view.findViewById(R.id.iv_avatar);
        userAttention = view.findViewById(R.id.iv_add_attention);
        userComment = view.findViewById(R.id.ll_comment);
        userShare = view.findViewById(R.id.ll_share);
        likeBtn = view.findViewById(R.id.like_btn);
        likeNumber = view.findViewById(R.id.tv_like_num);
        commentIcon = view.findViewById(R.id.iv_comment);
        commentNumber = view.findViewById(R.id.tv_comment_num);
        shareIcon = view.findViewById(R.id.iv_share);
        shareNumber = view.findViewById(R.id.tv_share_num);
        userIcon.setOnClickListener(this);
        userAttention.setOnClickListener(this);
        likeBtn.setOnCheckStateChangeListener(this);
        userComment.setOnClickListener(this);
        commentIcon.setOnClickListener(this);
        commentNumber.setOnClickListener(this);
        userShare.setOnClickListener(this);
        shareIcon.setOnClickListener(this);
        shareNumber.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    public void setRightMenuInfo(BaseVideoSourceModel info) {
        //设置头像
        String url = info.getHeadPictureUrl();
        String imgUrl = UrlUtils.isURL(url) ? url : UrlUtils.getURL(url);
        ImageLoaderOptions build = new ImageLoaderOptions.Builder().circle().error(R.mipmap.icon_avatar_change_def).build();
        new ImageLoaderImpl().loadImage(getContext(), imgUrl, build).into(userIcon);
        //是否关注
        userAttention.setStartDrawOk(info.isAttention() || info.isDelete());
        //是否点赞、点赞数
        likeBtn.setChecked(info.isFavorite());
        likeNumber.setText(info.getForwardingNumber() + "");
        //评论数
        commentNumber.setText(info.getUserCommentTotal() + "");
        //分享数
        shareNumber.setText(info.getSupport() + "");
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) return;
        int id = v.getId();
        if (id == R.id.iv_avatar) {
            mListener.onClick(v, MenuClick.ICON);
        } else if (id == R.id.iv_add_attention) {
            mListener.onClick(v, MenuClick.ATTENTION);
        } else if (id == R.id.ll_comment || id == R.id.iv_comment || id == R.id.tv_comment_num) {
            mListener.onClick(v, MenuClick.COMMENT);
        } else if (id == R.id.ll_share || id == R.id.iv_share || id == R.id.tv_share_num) {
            mListener.onClick(v, MenuClick.SHARE);
        }
    }

    @Override
    public void onCheckedChanged(View view, boolean checked) {
        if (mListener == null) return;
        mListener.onClick(view, checked ? MenuClick.LIKE : MenuClick.LIKE_UN);
    }

    /**
     * 是否显示关注按钮
     */
    public void setAttentionVisibility(int visibility) {
        userAttention.setVisibility(visibility);
    }

    public void setClickListener(OnMenuViewClick listener) {
        mListener = listener;
    }

    public interface OnMenuViewClick {
        void onClick(View view, MenuClick menuClick);
    }
}
