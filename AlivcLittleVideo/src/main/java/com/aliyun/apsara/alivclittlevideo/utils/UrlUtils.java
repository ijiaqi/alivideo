package com.aliyun.apsara.alivclittlevideo.utils;

import java.util.regex.Pattern;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2022/1/8 16:43
 * @Description :
 */
public class UrlUtils {

    public static boolean isURL(final CharSequence input) {
        String REGEX_URL = "[a-zA-z]+://[^\\s]*";
        return input != null && input.length() > 0 && Pattern.matches(REGEX_URL, input);
    }

    public static String getURL(final CharSequence input) {
        return HttpClientUtil.HOST_API + input;
    }
}
