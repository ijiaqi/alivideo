package com.aliyun.apsara.alivclittlevideo.view.video;

/**
 * VodFile上传的listener
 */
public interface OnVodFileUploadListener {
    /**
     * 上传成功
     *
     * @param videoId  videoId
     * @param imageUrl imageUrl
     */
    void onSuccess(String videoId, String imageUrl, String videoDescribe);

    /**
     * 上传失败
     *
     * @param code 错误码
     * @param msg  错误信息
     */
    void onFailure(String code, String msg);

    /**
     * 上传进度
     *
     * @param uploadedSize 已上传大小
     * @param totalSize    总大小
     */
    void onProgress(long uploadedSize, long totalSize);

    /**
     * 开始上传
     */
    void onStart();
}
