package com.aliyun.apsara.alivclittlevideo.view.video;

import android.view.View;
import android.widget.CompoundButton;

import com.aliyun.apsara.alivclittlevideo.view.video.weight.MenuClick;
import com.sackcentury.shinebuttonlib.ShineButton;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2021/11/3 16:56
 * @Description :
 */
public abstract class ItemBtnClickCallBack implements LittleVideoListAdapter.OnItemBtnClick {
    @Override
    public void onDownloadClick(int position) {

    }

    @Override
    public void onClickView(View view, int position) {

    }

    @Override
    public void onClickView(View view,MenuClick menuClick, int position) {

    }

    @Override
    public void onChecked(CompoundButton buttonView, boolean isChecked, int position) {

    }

    @Override
    public void onHeartClick(ShineButton heartButton, boolean isChecked, int position) {
    }

}
