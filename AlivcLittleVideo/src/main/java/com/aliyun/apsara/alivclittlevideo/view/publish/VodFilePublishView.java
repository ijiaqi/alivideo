package com.aliyun.apsara.alivclittlevideo.view.publish;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.alibaba.sdk.android.vod.upload.VODUploadCallback;
import com.alibaba.sdk.android.vod.upload.VODUploadClientImpl;
import com.alibaba.sdk.android.vod.upload.model.UploadFileInfo;
import com.alibaba.sdk.android.vod.upload.model.VodInfo;
import com.aliyun.apsara.alivclittlevideo.R;
import com.aliyun.apsara.alivclittlevideo.view.video.OnUploadCompleteListener;
import com.aliyun.common.utils.DensityUtil;
import com.aliyun.svideo.common.utils.image.ImageLoaderImpl;
import com.aliyun.svideo.common.utils.image.ImageLoaderOptions;
import com.aliyun.svideo.common.widget.AlivcCustomAlertDialog;

;
;

public class VodFilePublishView extends FrameLayout {
    /**
     * 时间戳错误
     */
    private static final String TEIM_STAMP_ERROR = "InvalidTimeStamp.Expired";
    private ProgressBar mProgressBar;
    private ImageView mIvThumbnail;
    private TextView mTvTip;
    /**
     * 封面图在屏幕中的宽度
     */
    private float mIvWidth = 67.5f;
    /**
     * 封面图在屏幕中的高度
     */
    private float mIvHeight = 120f;

    public VodFilePublishView(@NonNull Context context) {
        super(context);
        initView();
    }

    public VodFilePublishView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public VodFilePublishView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        mProgressBar = new ProgressBar(getContext(), null, android.R.attr.progressBarStyleHorizontal);
        mProgressBar.setProgressDrawable(getResources().getDrawable(R.drawable.vertical_progress_bar));
        mProgressBar.setProgress(100);
        mIvThumbnail = new ImageView(getContext());
        mIvThumbnail.setScaleType(ImageView.ScaleType.CENTER_CROP);
        mIvThumbnail.setBackgroundColor(getResources().getColor(R.color.alivc_common_bg_black_alpha_80));
        mTvTip = new TextView(getContext());
        mTvTip.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        mTvTip.setText(R.string.alivc_little_main_publish);
        mTvTip.setPadding(0, 0, 0, DensityUtil.dip2px(getContext(), 8));
        mTvTip.setTextColor(getResources().getColor(R.color.alivc_common_font_white));
        mTvTip.setTextSize(12);
    }

    public void showSuccess() {
        Log.d("UploadView", "showSuccess");
        //

        post(new Runnable() {
            @Override
            public void run() {
                clearUploadProgress();
                TextView textView = new TextView(getContext());
                textView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.alivc_common_bg_black_alpha_80));
                textView.setHeight(DensityUtil.dip2px(getContext(), 40));
                textView.setWidth(DensityUtil.dip2px(getContext(), 180));
                textView.setText(R.string.alivc_little_main_tip_upload_success);
                textView.setGravity(Gravity.CENTER);
                Toast toast = new Toast(getContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 70);
                toast.setView(textView);
                toast.show();
            }
        });
    }

    public void showFailed(final String message) {

        post(new Runnable() {
            @Override
            public void run() {
                clearUploadProgress();
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateProgress(final int progress) {
        post(new Runnable() {
            @Override
            public void run() {
                mProgressBar.setProgress(100 - progress);
            }
        });
    }

    public void showProgress2(String filePath) {
        new ImageLoaderImpl().loadVideoThumbnail(getContext(), filePath, mIvThumbnail);
        LayoutParams layoutParams = new LayoutParams(DensityUtil.dip2px(getContext(), mIvWidth),
                DensityUtil.dip2px(getContext(), mIvHeight));
        int statusBarHeight1 = -1;
        //获取status_bar_height资源的ID
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            statusBarHeight1 = getResources().getDimensionPixelSize(resourceId);
        } else {
            statusBarHeight1 = DensityUtil.dip2px(getContext(), 24);
        }
        layoutParams.setMargins(DensityUtil.dip2px(getContext(), 20), statusBarHeight1, 0, 0);
        if (mIvThumbnail.getParent() == null) {
            addView(mIvThumbnail, layoutParams);

        }
        if (mProgressBar.getParent() == null) {
            addView(mProgressBar, layoutParams);
        }
        if (mTvTip.getParent() == null) {
            addView(mTvTip, layoutParams);
        }
    }

    public void showProgress(String filePath) {
        new ImageLoaderImpl().loadImage(getContext(), filePath, new ImageLoaderOptions.Builder()
                //上传显示加载本地图片，路径可能会相同，这里需要跳过缓存
                .skipDiskCacheCache()
                .skipMemoryCache()
                .build()
        ).into(mIvThumbnail);
        LayoutParams layoutParams = new LayoutParams(DensityUtil.dip2px(getContext(), mIvWidth),
                DensityUtil.dip2px(getContext(), mIvHeight));
        int statusBarHeight1 = -1;
        //获取status_bar_height资源的ID
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            statusBarHeight1 = getResources().getDimensionPixelSize(resourceId);
        } else {
            statusBarHeight1 = DensityUtil.dip2px(getContext(), 24);
        }
        layoutParams.setMargins(DensityUtil.dip2px(getContext(), 20), statusBarHeight1, 0, 0);
        if (mIvThumbnail.getParent() == null) {
            addView(mIvThumbnail, layoutParams);

        }
        if (mProgressBar.getParent() == null) {
            addView(mProgressBar, layoutParams);
        }
        if (mTvTip.getParent() == null) {
            addView(mTvTip, layoutParams);
        }
    }

    public void clearUploadProgress() {
        removeView(mProgressBar);
        removeView(mIvThumbnail);
        removeView(mTvTip);
        mProgressBar.setProgress(100);
    }

    /**
     * 开始上传视频
     */
    public void startUploadVideo(
            String outputFilePath,
            String videoId,
            String videoAddress,
            String videoAuth,
            String videoDescribe) {
        VODUploadClientImpl vodUploadClient = new VODUploadClientImpl(getContext());
        VODUploadCallback callback = new VODUploadCallback() {
            @Override
            public void onUploadSucceed(UploadFileInfo info) {
                super.onUploadSucceed(info);
                if (mOutOnUploadCompleteListener != null) {
                    mOutOnUploadCompleteListener.onSuccess(videoId, "imageUrl", videoDescribe);
                }
            }

            @Override
            public void onUploadFailed(UploadFileInfo info, String code, String message) {
                super.onUploadFailed(info, code, message);
                if (TEIM_STAMP_ERROR.equals(code)) {
                    if (mOutOnUploadCompleteListener != null) {
                        mOutOnUploadCompleteListener.onFailure(code, message);
                    }
                    return;
                }
                showRetryDialog(code, message);
            }

            @Override
            public void onUploadProgress(UploadFileInfo info, long uploadedSize, long totalSize) {
                super.onUploadProgress(info, uploadedSize, totalSize);
                int progress = (int) ((uploadedSize * 100) / totalSize);
                Log.e("onUploadProgress", "progress" + progress);
                updateProgress(progress);
            }

            @Override
            public void onUploadStarted(UploadFileInfo uploadFileInfo) {
                super.onUploadStarted(uploadFileInfo);
                vodUploadClient.setUploadAuthAndAddress(uploadFileInfo, videoAuth, videoAddress);
                showProgress(outputFilePath);
            }
        };
        vodUploadClient.init(callback);
        VodInfo vodInfo = new VodInfo();
        vodInfo.setTitle("标题" + videoDescribe);
        vodInfo.setDesc("描述." + videoDescribe);
        vodUploadClient.addFile(outputFilePath, vodInfo);
        vodUploadClient.start();
    }

    public void showRetryDialog(final String code, final String message) {
        post(new Runnable() {
            @Override
            public void run() {
                clearUploadProgress();
                AlivcCustomAlertDialog dialog = new AlivcCustomAlertDialog.Builder(getContext())
                        .setMessage(message + "," + getResources().getString(R.string.alivc_little_main_dialog_upload_failed))
                        .setDialogClickListener(getResources().getString(R.string.alivc_little_main_dialog_retry), getResources().getString(R.string.alivc_little_main_dialog_close), new AlivcCustomAlertDialog.OnDialogClickListener() {
                            @Override
                            public void onConfirm() {
//                                mPublishManager.retryTask();
//                                showProgress(mPublishManager.getThumbnail());
                            }

                            @Override
                            public void onCancel() {
                                if (mOutOnUploadCompleteListener != null) {
                                    mOutOnUploadCompleteListener.onFailure(code, message);
                                }
                            }
                        })
                        .create();
                dialog.setCancelable(false);
                dialog.show();

            }
        });
    }


    public void onDestroy() {

    }

    private OnUploadCompleteListener mOutOnUploadCompleteListener;

    /**
     * 设置上传完成的listener
     *
     * @param listener OnUploadCompleteListener
     */
    public void setOnUploadCompleteListener(OnUploadCompleteListener listener) {
        this.mOutOnUploadCompleteListener = listener;
    }

}
