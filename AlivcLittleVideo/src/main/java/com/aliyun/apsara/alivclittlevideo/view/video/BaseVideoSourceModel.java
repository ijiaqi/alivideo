package com.aliyun.apsara.alivclittlevideo.view.video;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.aliyun.apsara.alivclittlevideo.view.video.videolist.IVideoSourceModel;
import com.aliyun.player.source.UrlSource;
import com.aliyun.player.source.VidSts;
import com.google.gson.annotations.SerializedName;

/**
 * 视频信息基类
 */
public abstract class BaseVideoSourceModel implements IVideoSourceModel {

    @Override
    public UrlSource getUrlSource() {
        return null;
    }

    @Override
    public VidSts getVidStsSource() {
        return null;
    }

    @Override
    public String getFirstFrame() {
        if (!TextUtils.isEmpty(firstFrameUrl)) {
            return firstFrameUrl;
        } else {
            return coverUrl;
        }
    }

    /**
     * id
     */
    protected String id = "";
    @SerializedName("userId")
    protected String userid = "";
    @SerializedName("userType")
    protected int type = 0;
    @SerializedName("userName")
    protected String nickName = "";
    @SerializedName("userHeadPic")
    protected String headPictureUrl = "";
    protected String teacherNickName = "";
    protected String teacherHeadPictureUrl = "";
    protected int userCommentTotal = 0;
    @SerializedName("forwardingTotal")
    protected int support = 0;
    protected int width = 1;
    protected int high = 1;
    @SerializedName("isAuthor")
    protected boolean isDelete = false;
    @SerializedName("isFavorite")
    protected boolean isFavorite = false;
    @SerializedName("isCollection")
    protected boolean isCollection = false;
    @SerializedName("isFollow")
    protected boolean isAttention = false;
    @SerializedName("isSelf")
    protected boolean isSelf = false;
    @SerializedName("favoriteTotal")
    protected int forwardingNumber = 0;
    protected int followStatus = 0;
    /**
     * 播放量
     */
    protected int viewing = 0;
    /**
     * 是否公开
     */
    @SerializedName("isPublic")
    protected boolean isPublic = false;
    /**
     * 是否置顶
     */
    @SerializedName("isTopping")
    protected boolean isTopping = false;
    /**
     * 是否可以下载
     */
    @SerializedName("isDownload")
    protected boolean isDownload = false;

    /**
     * 标题
     */
    @SerializedName("opusText")
    protected String title = "";
    /**
     * 描述
     */
    protected String description = "";

    /**
     * 封面url
     */
    @SerializedName("opusPic")
    protected String coverUrl = "";

    protected String video = "";
    /**
     * 创建时间
     */
    protected String creationTime = "";
    /**
     * 状态
     */
    protected String status = "";
    /**
     * 首帧地址
     */
    protected String firstFrameUrl = "";
    /**
     * 大小
     */
    protected int size = 0;
    /**
     * 分类id
     */
    protected int cateId = 0;
    /**
     * 分类名称
     */
    protected String cateName = "";
    /**
     * 标签
     */
    protected String tags = "";

    /**
     * 分享链接
     */
    protected String shareUrl = "";

    protected String teacherId;
    protected String teacherUserId;
    protected Float grade;
    protected String teacherName;
    @SerializedName("teacherCate")
    protected String categoryName;
    protected String teacherIconPicUrl = "";
    protected String longitude;
    protected String latitude;
    protected String businessName;
    protected String distance;

    /**
     * 视频作者
     */

    protected UserBean user;

//    public int getId() {
//        int i = 0;
//        try {
//            i = Integer.parseInt(id);
//        } catch (NumberFormatException e) {
//            e.printStackTrace();
//        }
//        return i;
//
//    }
//
//    public void setId(int id) {
//        this.id = id + "";
//    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadPictureUrl() {
        return headPictureUrl;
    }

    public void setHeadPictureUrl(String headPictureUrl) {
        this.headPictureUrl = headPictureUrl;
    }

    public String getTeacherNickName() {
        return teacherNickName;
    }

    public void setTeacherNickName(String teacherNickName) {
        this.teacherNickName = teacherNickName;
    }

    public String getTeacherHeadPictureUrl() {
        return teacherHeadPictureUrl;
    }

    public void setTeacherHeadPictureUrl(String teacherHeadPictureUrl) {
        this.teacherHeadPictureUrl = teacherHeadPictureUrl;
    }

    public int getUserCommentTotal() {
        return userCommentTotal;
    }

    public void setUserCommentTotal(int userCommentTotal) {
        this.userCommentTotal = userCommentTotal;
    }

    public int getSupport() {
        return support;
    }

    public void setSupport(int support) {
        this.support = support;
    }

    public int getForwardingNumber() {
        return forwardingNumber;
    }

    public void setForwardingNumber(int forwardingNumber) {
        this.forwardingNumber = forwardingNumber;
    }

    public boolean isAttention() {
        return isAttention;
    }

    public void setAttention(boolean attention) {
        isAttention = attention;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstFrameUrl() {
        return firstFrameUrl;
    }

    public void setFirstFrameUrl(String firstFrameUrl) {
        this.firstFrameUrl = firstFrameUrl;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public boolean isCollection() {
        return isCollection;
    }

    public void setCollection(boolean collection) {
        isCollection = collection;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setSelf(boolean self) {
        isSelf = self;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherUserId() {
        return teacherUserId;
    }

    public void setTeacherUserId(String teacherUserId) {
        this.teacherUserId = teacherUserId;
    }

    public Float getGrade() {
        return grade;
    }

    public void setGrade(Float grade) {
        this.grade = grade;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTeacherIconPicUrl() {
        return teacherIconPicUrl;
    }

    public void setTeacherIconPicUrl(String teacherIconPicUrl) {
        this.teacherIconPicUrl = teacherIconPicUrl;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(int followStatus) {
        this.followStatus = followStatus;
    }

    public int getViewing() {
        return viewing;
    }

    public void setViewing(int viewing) {
        this.viewing = viewing;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isTopping() {
        return isTopping;
    }

    public void setTopping(boolean topping) {
        isTopping = topping;
    }

    public boolean isDownload() {
        return isDownload;
    }

    public void setDownload(boolean download) {
        isDownload = download;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public static class UserBean implements Parcelable {

        private String userId;
        private String userName;
        private String avatarUrl;

        public UserBean(String userId, String nickName, String avatarUrl) {
            this.userId = userId;
            this.userName = nickName;
            this.avatarUrl = avatarUrl;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.userId);
            dest.writeString(this.userName);
            dest.writeString(this.avatarUrl);
        }

        public UserBean() {
        }

        protected UserBean(Parcel in) {
            this.userId = in.readString();
            this.userName = in.readString();
            this.avatarUrl = in.readString();
        }

        public static final Parcelable.Creator<UserBean> CREATOR = new Parcelable.Creator<UserBean>() {
            @Override
            public UserBean createFromParcel(Parcel source) {
                return new UserBean(source);
            }

            @Override
            public UserBean[] newArray(int size) {
                return new UserBean[size];
            }
        };
    }
}
