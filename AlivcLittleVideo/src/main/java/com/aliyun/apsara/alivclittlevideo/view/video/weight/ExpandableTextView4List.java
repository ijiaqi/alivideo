package com.aliyun.apsara.alivclittlevideo.view.video.weight;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2022/1/11 10:12
 * @Description :可展开折叠的文本控件（用于列表界面,如RecyclerView）
 */
public class ExpandableTextView4List extends ExpandableTextView {

    public ExpandableTextView4List(Context context) {
        this(context, null, 0);
    }

    public ExpandableTextView4List(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExpandableTextView4List(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setText(CharSequence text, boolean open) {
        mCancelAnim = true;
        super.setText(text, open);
        if (mMeasured) {
            toggleText();
        }
    }
}