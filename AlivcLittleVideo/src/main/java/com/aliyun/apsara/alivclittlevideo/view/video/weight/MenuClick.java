package com.aliyun.apsara.alivclittlevideo.view.video.weight;

/**
 * @ProjectName :ALiVideo
 * @Author : QI.JIA
 * @Time : 2022/1/8 16:30
 * @Description :视频右边菜单点击类型
 */
public enum MenuClick {
    /**
     * 头像
     */
    ICON,
    /**
     * 关注
     */
    ATTENTION,
    /**
     * 点赞
     */
    LIKE,
    /**
     * 取消点赞
     */
    LIKE_UN,
    /**
     * 评论
     */
    COMMENT,
    /**
     * 分享
     */
    SHARE,
    /**
     * 教师
     */
    TEACHER,
    /**
     * 静音
     */
    SILENCE,
    /**
     * 权限设置
     */
    AUTH_SETTING
}
