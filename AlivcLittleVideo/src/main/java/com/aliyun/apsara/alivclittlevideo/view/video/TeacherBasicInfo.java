package com.aliyun.apsara.alivclittlevideo.view.video;

import java.io.Serializable;

public class TeacherBasicInfo implements Serializable {
    protected String teacherId;
    protected String teacherUserId;
    protected Float grade;
    protected String teacherName;
    protected String teacherIconPicUrl="";
    protected String longitude;
    protected String latitude;
    protected String businessName;
    protected String distance;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherUserId() {
        return teacherUserId;
    }

    public void setTeacherUserId(String teacherUserId) {
        this.teacherUserId = teacherUserId;
    }

    public Float getGrade() {
        return grade;
    }

    public void setGrade(Float grade) {
        this.grade = grade;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherIconPicUrl() {
        return teacherIconPicUrl;
    }

    public void setTeacherIconPicUrl(String teacherIconPicUrl) {
        this.teacherIconPicUrl = teacherIconPicUrl;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
