package com.aliyun.apsara.alivclittlevideo.view.video;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.aliyun.apsara.alivclittlevideo.R;
import com.aliyun.apsara.alivclittlevideo.utils.HttpClientUtil;
import com.aliyun.apsara.alivclittlevideo.view.video.videolist.BaseVideoListAdapter;
import com.aliyun.apsara.alivclittlevideo.view.video.videolist.FollowUser;
import com.aliyun.apsara.alivclittlevideo.view.video.weight.MenuClick;
import com.aliyun.apsara.alivclittlevideo.view.video.weight.VideoInfoView;
import com.aliyun.apsara.alivclittlevideo.view.video.weight.VideoRightMenuView;
import com.aliyun.svideo.common.utils.image.ImageLoaderImpl;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.regex.Pattern;

/**
 * 视频列表的adapter
 *
 * @author xlx
 */
public class LittleVideoListAdapter extends BaseVideoListAdapter<LittleVideoListAdapter.MyHolder, BaseVideoSourceModel> {
    public static final String TAG = LittleVideoListAdapter.class.getSimpleName();
    private OnItemBtnClick mItemBtnClick;
    private MyHolder mHolder;
    private int mLayout = R.layout.item_view_pager;
    private int mBottomCommentVisibility = View.GONE;

    public LittleVideoListAdapter(Context context) {
        super(context);
    }

    public LittleVideoListAdapter(Context context, @LayoutRes int layout) {
        super(context);
        mLayout = layout;
    }


    public void setBottomCommentVisible(int visible) {
        mBottomCommentVisibility = visible;
    }

    @NonNull
    @Override
    public LittleVideoListAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(mLayout, viewGroup, false);
        return new MyHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder myHolder, @SuppressLint("RecyclerView") final int position) {
        super.onBindViewHolder(myHolder, position);
        initView(myHolder, list.get(position));
        BaseVideoSourceModel video = list.get(position);
        //绑定点击事件
        myHolder.mRightMenuView.setClickListener((view, menuClick) -> {
            if (mItemBtnClick == null) return;
            switch (menuClick) {
                case ICON:
                case ATTENTION:
                case COMMENT:
                case SHARE:
                    mItemBtnClick.onClickView(view, menuClick, position);
                    break;
                case LIKE:
                    mItemBtnClick.onHeartClick((ShineButton) view, true, position);
                    break;
                case LIKE_UN:
                    mItemBtnClick.onHeartClick((ShineButton) view, false, position);
                    break;
            }
        });
        myHolder.mVideoInfoView.setInfoListener((view, menuClick) -> {
            if (mItemBtnClick == null) return;
            switch (menuClick) {
                case TEACHER:
                    mItemBtnClick.onClickView(view, menuClick, position);
                    break;
                case SILENCE:
                    AppCompatCheckBox cb = (AppCompatCheckBox) view;
                    mItemBtnClick.onChecked(cb, cb.isChecked(), position);
                    break;
            }
        });
        myHolder.bottomComment.setOnClickListener(v -> {
            mItemBtnClick.onClickView(v, MenuClick.COMMENT, position);
        });
        myHolder.tvAuthSetting.setOnClickListener(v -> {
            mItemBtnClick.onClickView(v, MenuClick.AUTH_SETTING, position);
        });
    }

    public static boolean isURL(final CharSequence input) {
        String REGEX_URL = "[a-zA-z]+://[^\\s]*";
        return input != null && input.length() > 0 && Pattern.matches(REGEX_URL, input);
    }

    public static String getURL(final CharSequence input) {
        return HttpClientUtil.HOST_API + input;
    }

    public void refreshData(RecyclerView.ViewHolder holder, BaseVideoSourceModel video) {
        if (holder != null && holder instanceof MyHolder)
            initView((MyHolder) holder, video);
    }

    public MyHolder getmHolder() {
        return mHolder;
    }

    private void initView(MyHolder holder, BaseVideoSourceModel model) {
        String urlThumb = model.getCoverUrl();
        String imgUrlThumb = isURL(urlThumb) ? urlThumb : getURL(urlThumb);
        new ImageLoaderImpl().loadImage(context, imgUrlThumb).into(holder.thumb);
        holder.mRightMenuView.setRightMenuInfo(model);
        holder.mRightMenuView.setAttentionVisibility(isAttention(model.getUserid(), model.getType()));
        holder.mVideoInfoView.setOpusTimeVisible(mBottomCommentVisibility);
        holder.mVideoInfoView.setVideoInfo(model, holder.getAdapterPosition());
        //需要显示底部评论，不是当前用户发布的作品
        holder.bottomComment.setVisibility(isBottomVisible(model.isDelete, mBottomCommentVisibility));
        holder.llAuthorBottom.setVisibility(isBottomVisible(model.isDelete));
        holder.tvPlayCount.setText(model.viewing + "浏览");
    }

    private int isBottomVisible(boolean isAuthor) {
        return isAuthor ? View.VISIBLE : View.GONE;
    }

    /**
     * 是否显示底部评论入口
     *
     * @param isAuthor 是否作者
     * @param visible  场景是否显示
     * @return
     */
    private int isBottomVisible(boolean isAuthor, int visible) {
        int isVisible;
        //不是作者，需显示场景
        if (!isAuthor && visible == View.VISIBLE) isVisible = View.VISIBLE;
            //不是作者，不需显示场景
        else if (!isAuthor) isVisible = View.GONE;
            //是作者，需显示场景
        else if (visible == View.VISIBLE) isVisible = View.INVISIBLE;
            //是作者，不需显示场景
        else isVisible = View.INVISIBLE;
        return isVisible;
    }

    public int isAttention(String userId, int userType) {
        FollowUser user = new FollowUser(userId, userType);
        return mFollowUsers.contains(user) ? View.GONE : View.VISIBLE;
    }

    public final class MyHolder extends BaseVideoListAdapter.BaseHolder {
        private ImageView thumb;
        public FrameLayout playerView;
        private ViewGroup mRootView;
        private ImageView mPlayIconImageView;
        private VideoRightMenuView mRightMenuView;
        private VideoInfoView mVideoInfoView;
        private SeekBar videoProgress;
        private TextView bottomComment;
        private LinearLayout llAuthorBottom;
        private TextView tvPlayCount;
        private TextView tvAuthSetting;

        MyHolder(@NonNull View itemView) {
            super(itemView);
            Log.d(TAG, "new PlayerManager");
            thumb = itemView.findViewById(R.id.img_thumb);
            playerView = itemView.findViewById(R.id.player_view);
            mRootView = itemView.findViewById(R.id.root_view);
            mPlayIconImageView = itemView.findViewById(R.id.iv_play_icon);
            mRightMenuView = itemView.findViewById(R.id.video_right_menu);
            mVideoInfoView = itemView.findViewById(R.id.video_info);
            videoProgress = itemView.findViewById(R.id.video_progress);
            bottomComment = itemView.findViewById(R.id.tv_comment);
            llAuthorBottom = itemView.findViewById(R.id.ll_author_bottom);
            tvPlayCount = itemView.findViewById(R.id.tv_play_count);
            tvAuthSetting = itemView.findViewById(R.id.tv_auth_setting);
        }

        @Override
        public ImageView getCoverView() {
            return thumb;
        }

        @Override
        public ViewGroup getContainerView() {
            return mRootView;
        }

        @Override
        public ImageView getPlayIcon() {
            return mPlayIconImageView;
        }

        @Override
        public SeekBar getVideoSeekBar() {
            return videoProgress;
        }

        @Override
        public TextView getBottomComment() {
            return bottomComment;
        }
    }

    public interface OnItemBtnClick {
        void onDownloadClick(int position);

        void onClickView(View view, int position);

        void onClickView(View view, MenuClick menuClick, int position);

        void onChecked(CompoundButton buttonView, boolean isChecked, int position);

        void onHeartClick(ShineButton heartButton, boolean isChecked, int position);

    }

    public void setItemBtnClick(
            OnItemBtnClick mItemBtnClick) {
        this.mItemBtnClick = mItemBtnClick;
    }


}
